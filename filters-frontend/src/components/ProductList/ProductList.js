import React, { useEffect, useState } from 'react'
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import axios from 'axios';
import StarRateRoundedIcon from '@mui/icons-material/StarRateRounded';
import { TablePagination } from '@mui/material';
import './ProductList.css';

export const ProductList = ({ query }) => {
  const [productList, setProductList] = useState([]);
  
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(-1);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  }

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(event.target.value, 10);
    setPage(0);
  }

  const getProducts = (q) => {
    axios({
        method: 'get',
        url: `http://127.0.0.1:8000/products${q}&page=${page}&pageSize=${rowsPerPage}`,
    }).then((response) => {
        setProductList(response.data.result);
        setCount(response.data.count);
    });
  }

  useEffect(()=>{
    if(query){
      getProducts(query);
    }
  }, [page, rowsPerPage]);

  useEffect(() => {
    if(query){
      getProducts(query);
      setPage(0);
    }
  }, [query]);

  return (
    <div id='product-list'>
      <TablePagination
          component="div"
          count={count}
          page={page}
          onPageChange={handleChangePage}
          rowsPerPage={rowsPerPage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          style={{width: '95%', marginBottom: '2%'}}
        />
      <Grid item xs={12}>
        <Grid container justifyContent="center" spacing={12}>
          {productList.map((value) => (
            <Grid key={value.id} item>
              <Card sx={{ width: 345 }}>
                <CardMedia
                  sx={{ height: 140 }}
                  image={value.thumbnail}
                  title={value.title}
                />
                <CardContent>
                <Typography variant="body1" color="text.primary">
                    {value.brand}
                  </Typography>
                  <Typography gutterBottom variant="h5" component="div">
                    {value.title}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    {value.description}
                  </Typography>
                </CardContent>
                <CardActions>
                  <div id='foot'>
                    <span>${value.price}</span>
                    <span id="discount">{value.discountPercentage}% off</span>
                    <span id="rating"><StarRateRoundedIcon htmlColor='#D5AB55' />{value.rating}</span>
                  </div>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </div>
  )
}
