import axios from 'axios';
import React, { useEffect, useState } from 'react';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import ListItemText from '@mui/material/ListItemText';
import Select from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';
import CheckIcon from '@mui/icons-material/Check';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import './headerStyle.css';

export const Header = ({ setQuery }) => {
    // Filter Data
    const ratings = [
        {value: 1, label: '1+'},
        {value: 1.5, label: '1.5+'},
        {value: 2, label: '2+'},
        {value: 2.5, label: '2.5+'},
        {value: 3, label: '3+'},
        {value: 3.5, label: '3.5+'},
        {value: 4, label: '4+'},
        {value: 4.5, label: '4.5+'},
        {value: 5, label: '5'},
    ];
    const MenuProps = {
        PaperProps: {
          style: {
            maxHeight: 48 * 4.5 + 8,
            width: 250,
          },
        },
      };
    const [price, setPrice] = useState([0,2000]);
    const [dp, setDp] = useState('');
    const [selectedRating, setSelectedRating] = useState('');
    const [categories, setCategories] = useState([]);
    const [selectedCategories, setSelectedCategories] = useState([]);
    const [brands, setBrands] = useState([]);
    const [selectedBrands, setSelectedBrands] = useState([]);
    const [isInStock, setIsInStock] = useState(false);

    useEffect(()=>{
        let query=`?stock=${isInStock}&minp=${price[0]}&maxp=${price[1]}`;
        selectedCategories.forEach((i)=>{
            query+=`&category=${i}`;
        });
        selectedBrands.forEach((i)=>{
            query+=`&brand=${i}`;
        });
        if(selectedRating!==''){
            query+=`&rating=${selectedRating}`;
        }
        if(dp!==''){
            query+=`&discountPercentage=${dp}`;
        }
        setQuery(query);
    }, [price, dp, selectedRating, selectedBrands, selectedCategories, isInStock]);

    const getBrandFilter = () => {
        let url = 'http://127.0.0.1:8000/brands';
        if(selectedCategories.length!==0){
            url += '?category=';
            selectedCategories.forEach((item, index) => {
                url+=item;
                if(index!==selectedCategories.length-1){
                    url+=','
                }
            })
        }
        axios({
            method: 'get',
            url: url,
        }).then((response) => {
            setBrands(response.data.result);
        });
    }

    useEffect(() => {
        getBrandFilter();
    }, [categories]);

    const getCategoryFilter = async () => {
        axios({
            method: 'get',
            url: 'http://127.0.0.1:8000/categories',
        }).then((response) => {
            // console.log(response.data);
            setCategories(response.data.result);
        });
    }

    useEffect(() => {
        getCategoryFilter();
    }, []);

    useEffect(()=>{
        setSelectedBrands([]);
        getBrandFilter();
    }, [selectedCategories]);
    
    const handleBrandChange = (e) => {
        setSelectedBrands(e.target.value);
    }

    const handleCategoryChange = (e) => {
        setSelectedCategories(e.target.value);
    }

    const handleRatingChange = (e) => {
        setSelectedRating(e.target.value);
    }

    const handleDpChange = (e) => {
        setDp(e.target.value);
    }

    const handlePriceChange = (e, newValue) => {
        setPrice(newValue);
    }

  return (
    <div id='header'>
        <nav>
            AmazeKart
        </nav>
        <div id='filters'>
            <Box sx={{ width: 200 }}>
                Price Range
                <Slider
                    getAriaLabel={() => 'Price Range'}
                    value={price}
                    onChange={handlePriceChange}
                    valueLabelDisplay="auto"
                    min={0}
                    max={2000}
                />
            </Box>
            <ToggleButtonGroup 
                size="large"
                aria-label="Large sizes"
                exclusive={true}
                value={dp}
                onChange={handleDpChange}
            >
                <ToggleButton value="5+" key="5+">
                    5% above
                </ToggleButton>
                <ToggleButton value="10+" key="10+">
                    10% above
                </ToggleButton>
                <ToggleButton value="15+" key="15+">
                    15% above
                </ToggleButton>
                <ToggleButton value="20+" key="20+">
                    20% above
                </ToggleButton>
            </ToggleButtonGroup>
            <FormControl sx={{m: 1}}>
                <ToggleButton
                    value="check"
                    selected={isInStock}
                    onChange={() => {
                        setIsInStock(!isInStock);
                    }}
                    >
                    Only in Stock options
                </ToggleButton>
            </FormControl>
            <FormControl sx={{m: 1, width: 200}}>
                <InputLabel id="demo-simple-select-label">Rating</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selectedRating}
                    label="Rating"
                    onChange={handleRatingChange}
                >
                    {ratings.map((item) => (
                        <MenuItem value={item.value}>{item.label}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl sx={{ m: 1, width: 200 }}>
                <InputLabel id="demo-multiple-checkbox-label">Brands</InputLabel>
                <Select
                    labelId="demo-multiple-checkbox-label"
                    id="demo-multiple-checkbox"
                    multiple
                    value={selectedBrands}
                    onChange={handleBrandChange}
                    input={<OutlinedInput label="Tag" />}
                    renderValue={(selected) => selected.join(', ')}
                    MenuProps={MenuProps}
                    >
                    {brands.map((name) => (
                        <MenuItem key={name} value={name}>
                        <Checkbox checked={selectedBrands.indexOf(name) > -1} />
                        <ListItemText primary={name} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl sx={{ m: 1, width: 200 }}>
                <InputLabel id="demo-multiple-checkbox-label">Categories</InputLabel>
                <Select
                    labelId="demo-multiple-checkbox-label"
                    id="demo-multiple-checkbox"
                    multiple
                    value={selectedCategories}
                    onChange={handleCategoryChange}
                    input={<OutlinedInput label="Tag" />}
                    renderValue={(selected) => selected.join(', ')}
                    MenuProps={MenuProps}
                    >
                    {categories.map((item) => (
                        <MenuItem key={item} value={item}>
                        <Checkbox checked={selectedCategories.indexOf(item) > -1} />
                        <ListItemText primary={item} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    </div>
  )
}
