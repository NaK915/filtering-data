import { useEffect, useState } from 'react';
import './App.css';
import { Header } from './components/Header/Header';
import { ProductList } from './components/ProductList/ProductList';

function App() {
  const [query, setQuery] = useState('');
  // useEffect(()=>{
  //   console.log(query);
  // }, [query]);
  return (
    <div className="App">
      <Header setQuery={setQuery}/>
      <ProductList query={query}/>
    </div>
  );
}

export default App;
