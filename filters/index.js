// Required Node Modules
const express = require('express');
const path = require('path');
const cors = require('cors');
const mongoose = require('mongoose');
require('dotenv').config();

// Product Model
const Product = require('./models/Product');

// Setting up Express
const app = express();
app.use(cors());
app.use(express.urlencoded({extended: false}));

// Connecting to MongoDB
mongoose.connect(process.env.MONGO_URL).then(() => {
    console.log('Connected!');
});

// Test Route
app.get('/', (req, res) => {
    res.send('Hello World!').end();
});

// Get Category Filters 
app.get('/categories', async (req, res) => {
    try{
        const dbData = await Product.find();
        const tempSet = new Set();
        dbData.forEach((item)=>{
            tempSet.add(item.category);
        });       
        const out = {
            count: tempSet.size,
            result: Array.from(tempSet)
        }
        res.json(out);
        res.status(200).end();
    } catch(err) {
        console.log(err);
        res.status(500).end();
    }
});

// Get brand Filters (dependent on category filters)
app.get('/brands', async (req, res) => {
    const category = req.query.category?.split(',');
    try{
        const dbData = category === undefined ? await Product.find() : await Product.find({category: category});
        const tempSet = new Set();
        dbData.forEach((item)=>{
            tempSet.add(item.brand);
        })
        const out = {
            count: tempSet.size,
            result: Array.from(tempSet)
        }
        res.json(out);
        res.status(200).end();
    } catch(err) {
        console.log(err);
        res.status(500).end();
    }
});

// Main Product Filter route
app.get('/products', async (req, res) => {
    // We can use various filters for sorting data here
    // Price, discount-percentage, rating, out/in-stock, brand, category

    const query = req.query;
    const pageSize = Number(query.pageSize);
    const skip = Number(query.page)*pageSize;
    const mquery = {};
    console.log(query);
    if(query.stock==='true'){
        mquery.stock={$gt: 0};
    } else {
        delete mquery.stock;
    }
    if(query.minp && query.maxp){
        mquery.price={$gte: Number(query.minp), $lte: Number(query.maxp)};
    }
    if(query.rating!==undefined){
        mquery.rating={$gte: Number(query.rating)};
    }
    if(query.discountPercentage!==undefined){
        mquery.discountPercentage={$gte: Number(query.discountPercentage)};
    }
    if(query.brand){
        mquery.brand=query.brand;
    }
    if(query.category){
        mquery.category=query.category;
    }
    console.log(mquery);

    try{
        const dbData = await Product.find(mquery);
        const out = {
            count: dbData.length,
            result: dbData.slice(skip, skip+pageSize)
        }
        res.json(out);
        res.status(200).end();
    } catch(err) {
        console.log(err);
        res.status(500).end();
    }
});

// App listening
app.listen(process.env.PORT, () => {
    console.log(`Example app listening on port ${process.env.PORT}`);
});

// Price - 0 to 1749
// dis - 0 to 17.94
// rating - 0 to 4.99
// brand - [
//     "Apple",
//     "Samsung",
//     "OPPO",
//     "Huawei",
//     "Microsoft Surface",
//     "Infinix",
//     "HP Pavilion",
//     "Impression of Acqua Di Gio",
//     "Royal_Mirage",
//     "Fog Scent Xpressio",
//     "Al Munakh",
//     "Lord - Al-Rehab",
//     "L'Oreal Paris",
//     "Hemani Tea",
//     "Dermive",
//     "ROREC White Rice",
//     "Fair & Clear",
//     "Saaf & Khaas",
//     "Bake Parlor Big",
//     "Baking Food Items",
//     "fauji",
//     "Dry Rose",
//     "Boho Decor",
//     "Flying Wooden",
//     "LED Lights",
//     "luxury palace",
//     "Golden",
//     "Furniture Bed Set",
//     "Ratttan Outdoor",
//     "Kitchen Shelf",
//     "Multi Purpose",
//     "AmnaMart",
//     "Professional Wear",
//     "Soft Cotton",
//     "Top Sweater",
//     "RED MICKY MOUSE..",
//     "Digital Printed",
//     "Ghazi Fabric",
//     "IELGY",
//     "IELGY fashion",
//     "Synthetic Leather",
//     "Sandals Flip Flops",
//     "Maasai Sandals",
//     "Arrivals Genuine",
//     "Vintage Apparel",
//     "FREE FIRE",
//     "The Warehouse",
//     "Sneakers",
//     "Rubber",
//     "Naviforce",
//     "SKMEI 9117",
//     "Strap Skeleton",
//     "Stainless",
//     "Eastern Watches",
//     "Luxury Digital",
//     "Watch Pearls",
//     "Bracelet",
//     "LouisWill",
//     "Copenhagen Luxe",
//     "Steal Frame",
//     "Darojay",
//     "Fashion Jewellery",
//     "Cuff Butterfly",
//     "Designer Sun Glasses",
//     "mastar watch",
//     "Car Aux",
//     "W1209 DC12V",
//     "TC Reusable",
//     "Neon LED Light",
//     "METRO 70cc Motorcycle - MR70",
//     "BRAVE BULL",
//     "shock absorber",
//     "JIEPOLLY",
//     "Xiangle",
//     "lightingbrilliance",
//     "Ifei Home",
//     "DADAWU",
//     "YIOSI"
// ]

// category - [
//     "smartphones",
//     "laptops",
//     "fragrances",
//     "skincare",
//     "groceries",
//     "home-decoration",
//     "furniture",
//     "tops",
//     "womens-dresses",
//     "womens-shoes",
//     "mens-shirts",
//     "mens-shoes",
//     "mens-watches",
//     "womens-watches",
//     "womens-bags",
//     "womens-jewellery",
//     "sunglasses",
//     "automotive",
//     "motorcycle",
//     "lighting"
// ]